import time
import tensorflow as tf
import tensorflow.keras as keras
import numpy as np


class TripletLoss_RNN_Network:
    #gets standard words as inputs. Returns a numpy array with zero padding on the right side.
    def preprocess_data(self,data):
        maxlen=-1
        for x in data:
            maxlen=max(maxlen,len(x))
        maxlen=max(35,maxlen)
        data2=np.zeros(shape=[len(data), maxlen], dtype=np.int32)
        for i,x in enumerate(data):
            for j,y in enumerate(x):
                data2[i][j]=self.alphabet2[y]+1 #+1 so that 0 remains only on the masked positions
        return data2

    def __init__(self, name,params=None):
        if(params is None):
            params={"margin":0.5-0.4, "representation_net":"cnn2", "representation_net_params":None, "learning_rate_decay_by":0.5**0.25, "learning_rate_decay_every":5-5 }
        self.epoch=0
        self.params=params
        if(params["representation_net"]=="cnn1"):
            params["representation_net"]=self.cnn1
        elif(params["representation_net"]=="cnn2"):
            params["representation_net"]=self.cnn2
        elif(params["representation_net"]=="gru"):
            params["representation_net"]=self.gru
        else:
            raise NotImplementedError("Unknown net")


        self.global_step=1
        self.alphabet="^$qwertyuiopasdfghjklzxcvbnmméěřťžúůíóášďýčň"
        self.alphabet2={}

        i=0
        while i<len(self.alphabet):
            self.alphabet2[self.alphabet[i]]=i
            i+=1



        self.graph = tf.Graph()
        self.session = tf.compat.v1.Session(graph=self.graph,config=tf.compat.v1.ConfigProto(allow_soft_placement=True,log_device_placement=False))
        self.build_net(params)

        with self.session.as_default(), self.graph.as_default():
            init = tf.compat.v1.global_variables_initializer()
            self.session.run(init)
            self.saver = tf.compat.v1.train.Saver(tf.compat.v1.trainable_variables())

    gru_=None
    def gru(self, input_tensor, params, mask=None):
        if(params==None):
            params={"state_size":200}
        if(self.gru_==None):
            with self.session.as_default(), self.graph.as_default():
                self.gru_=tf.keras.layers.GRU(params["state_size"], return_sequences=False, return_state=True, name="GRU1")
        states, final_state=self.gru_(input_tensor)
        return final_state

    cnn2_=None
    def cnn2(self, input_tensor, params,mask=None): #what about a simple one layer cnn? then reducemax on activations of each filter on different positions
        if(self.cnn2_==None):
            self.cnn2_=[tf.keras.layers.Conv1D(filters=200,kernel_size=4,strides=1,activation=tf.math.tanh, use_bias=True),
                        tf.keras.layers.Dense(200,use_bias=True, activation=tf.math.tanh),
                        lambda x: tf.reduce_max(x,axis=-2)
                        ]

        input=input_tensor
        for layer in self.cnn2_:
            input=layer(input)
        return input

    cnn1_=None
    def cnn1(self, input_tensor, params,mask=None): #what about a simple one layer cnn? then reducemax on activations of each filter on different positions
        if(self.cnn1_==None):
            self.cnn1_=[tf.keras.layers.Conv1D(filters=200,kernel_size=4,strides=1,activation=tf.math.tanh, use_bias=True),
                        lambda x: tf.reduce_max(x,axis=-2),
                        tf.keras.layers.Dense(100,use_bias=True, activation=tf.math.tanh)]

        input=input_tensor
        for layer in self.cnn1_:
            input=layer(input)
        return input

    def build_net(self, PARAMS):
        with self.session.as_default(), self.graph.as_default():
            net=lambda x,mask: PARAMS["representation_net"](x, params=PARAMS["representation_net_params"],mask=mask)
            self.learning_rate=tf.Variable(0.001, dtype=np.float32, trainable=False, name="LearningRate")
            self.distance_threshold=tf.Variable(PARAMS["margin"], dtype=np.float32, trainable=False, name="DistanceThreshold")
            self.character_embeddings=keras.layers.Embedding(len(self.alphabet)+1, 10, mask_zero=True,name="Embeddings");


            #Placeholders
            self.central_samples =tf.keras.backend.placeholder(shape=[None,35], dtype=tf.float32,name="CentralSamples")
            self.positive_samples=tf.keras.backend.placeholder(shape=[None,35], dtype=tf.float32,name="PositiveSamples")
            self.negative_samples=tf.keras.backend.placeholder(shape=[None,35], dtype=tf.float32,name="NegativeSamples")

            #Compute representations
            mask_central = self.character_embeddings.compute_mask(self.central_samples)
            self.representation_central =net(self.character_embeddings(self.central_samples ), mask=mask_central)

            mask_positive = self.character_embeddings.compute_mask(self.positive_samples)
            self.representation_positive=net(self.character_embeddings(self.positive_samples), mask=mask_positive)

            mask_negative = self.character_embeddings.compute_mask(self.negative_samples)
            self.representation_negative=net(self.character_embeddings(self.negative_samples), mask=mask_negative)

            #Compute distances in pairs.
            self.positive_distance=tf.reduce_mean((self.representation_central-self.representation_positive)**2, axis=-1)
            self.negative_distance=tf.reduce_mean((self.representation_central-self.representation_negative)**2, axis=-1)


            #Compute loss and prepair training.
            self.loss=tf.reduce_mean(tf.maximum(self.positive_distance-self.negative_distance+self.distance_threshold,0))
            self.optimizer=tf.compat.v1.train.AdamOptimizer(learning_rate=self.learning_rate)
            self.training=self.optimizer.minimize(self.loss) #, var_list=tf.compat.v1.trainable_variables())


    def compute_representations(self, data):
        return self.session.run([self.representation_positive], feed_dict={self.positive_samples:data})[0]


    def load(self, fname):
        with self.session.as_default(), self.graph.as_default():
            self.saver.restore(self.session,fname)