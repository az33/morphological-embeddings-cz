import TripletLossNetworkModel
network=TripletLossNetworkModel.TripletLoss_RNN_Network("tri-MixedTraining")
network.load("nets/tf_tripletloss_model_76perc-4761")

def Embed(list_of_words):
    global network
    if(type(list_of_words)==str):
        raise Exception("Embed expects list of words, not a single word")
    return network.compute_representations(network.preprocess_data(list_of_words))